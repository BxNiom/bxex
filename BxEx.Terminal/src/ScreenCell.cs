namespace BxEx.Terminal;

public class ScreenCell
{
    private TerminalColor _background;
    private TerminalColor _foreground;
    private char _symbol;

    internal ScreenCell(int x, int y, TerminalColor foreground, TerminalColor background, char symbol)
    {
        X = x;
        Y = y;
        Foreground = foreground;
        Background = background;
        Symbol = symbol;
        Changed = false;
    }

    public int X { get; }
    public int Y { get; }

    public TerminalColor Foreground
    {
        get => _foreground;
        set
        {
            if (_foreground != value)
            {
                _foreground = value;
                Changed = true;
            }
        }
    }

    public TerminalColor Background
    {
        get => _background;
        set
        {
            if (_background != value)
            {
                _background = value;
                Changed = true;
            }
        }
    }

    public char Symbol
    {
        get => _symbol;
        set
        {
            if (_symbol != value)
            {
                _symbol = value;
                Changed = true;
            }
        }
    }

    public bool Changed { get; internal set; }
}