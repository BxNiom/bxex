namespace BxEx.Terminal;

public readonly struct TerminalColor
{
    public string Foreground { get; }
    public string Background { get; }

    private TerminalColor(string foreground, string background)
    {
        Background = background;
        Foreground = foreground;
    }

    public static TerminalColor From4Bit(int colorCode, bool isBright)
    {
        return new TerminalColor($"\033[{colorCode + (isBright ? 90 : 30)}m",
            $"\033[{colorCode + (isBright ? 100 : 40)}m");
    }

    public static TerminalColor From8Bit(int colorCode)
    {
        return new TerminalColor($"\033[38;5;{colorCode}m", $"\033[48;5;{colorCode}m");
    }

    public bool Equals(TerminalColor other)
    {
        return Foreground.Equals(other.Foreground, StringComparison.OrdinalIgnoreCase) &&
               Background.Equals(other.Background, StringComparison.OrdinalIgnoreCase);
    }

    public override bool Equals(object? obj)
    {
        return obj is TerminalColor other && Equals(other);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Foreground, Background);
    }

    public static bool operator ==(TerminalColor a, TerminalColor b) => a.Equals(b);
    public static bool operator !=(TerminalColor a, TerminalColor b) => !a.Equals(b);

    public static TerminalColor Black { get; } = From4Bit(0, false);
    public static TerminalColor Red { get; } = From4Bit(1, false);
    public static TerminalColor Green { get; } = From4Bit(2, false);
    public static TerminalColor Yellow { get; } = From4Bit(3, false);
    public static TerminalColor Blue { get; } = From4Bit(4, false);
    public static TerminalColor Magenta { get; } = From4Bit(5, false);
    public static TerminalColor Cyan { get; } = From4Bit(6, false);
    public static TerminalColor White { get; } = From4Bit(7, false);

    public static TerminalColor BrightBlack { get; } = From4Bit(0, true);
    public static TerminalColor BrightRed { get; } = From4Bit(1, true);
    public static TerminalColor BrightGreen { get; } = From4Bit(2, true);
    public static TerminalColor BrightYellow { get; } = From4Bit(3, true);
    public static TerminalColor BrightBlue { get; } = From4Bit(4, true);
    public static TerminalColor BrightMagenta { get; } = From4Bit(5, true);
    public static TerminalColor BrightCyan { get; } = From4Bit(6, true);
    public static TerminalColor BrightWhite { get; } = From4Bit(7, true);

    public static TerminalColor NavyBlue { get; } = From8Bit(17);
    public static TerminalColor DarkBlue { get; } = From8Bit(18);
    public static TerminalColor DarkGreen { get; } = From8Bit(22);
    public static TerminalColor DarkCyan { get; } = From8Bit(36);
    public static TerminalColor DeepBlueSky { get; } = From8Bit(39);
    public static TerminalColor DarkTurquoise { get; } = From8Bit(44);
    public static TerminalColor DarkRed { get; } = From8Bit(52);
    public static TerminalColor DarkMagenta { get; } = From8Bit(90);
    public static TerminalColor DarkViolet { get; } = From8Bit(92);
    public static TerminalColor IndianRed { get; } = From8Bit(131);
    public static TerminalColor DarkGoldenrod { get; } = From8Bit(136);
    public static TerminalColor DarkKhaki { get; } = From8Bit(143);
    public static TerminalColor GreenYellow { get; } = From8Bit(154);
    public static TerminalColor Orchid { get; } = From8Bit(170);
    public static TerminalColor Orange { get; } = From8Bit(172);
    public static TerminalColor Violet { get; } = From8Bit(177);
    public static TerminalColor Tan { get; } = From8Bit(180);
    public static TerminalColor DarkOrange { get; } = From8Bit(208);
}