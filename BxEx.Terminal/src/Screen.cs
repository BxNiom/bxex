namespace BxEx.Terminal;

public class TerminalScreen
{
    private ScreenCell[][] _cells;

    public TerminalScreen()
    {
    }

    public int Width { get; private set; }
    public int Height { get; private set; }
}