using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace BxEx;

public static class StringEx
{
    public static Tuple<string, string>[] HtmlTags = new[]
    {
        new Tuple<string, string>("&auml;", "ä"),
        new("&ouml;", "ö"),
        new("&uuml;", "ü"),
        new("&Auml;", "Ä"),
        new("&Ouml;", "Ö"),
        new("&Uuml;", "Ü"),
        new("&ldquo;", "\""),
        new("&rdquo;", "\""),
        new("&amp;", "&"),
        new("&lt;", "<"),
        new("&gt;", ">"),
        new("&euro;", "€"),
        new("&sect;", "§"),
        new("&dollar;", "$"),
        new("&szlig;", "ss"),
        new("&szlig;", "ß"),
        new("&nbsp;", " "),
    };

    public static byte[] GetMD5Hash(this string s, Encoding? encoding = null) =>
        MD5.HashData((encoding ?? Encoding.UTF8).GetBytes(s));

    public static string GetMD5HashStr(this string s, Encoding? encoding = null) =>
        BitConverter.ToString(s.GetMD5Hash(encoding)).Replace("-", "");

    public static string Truncate(this string s, int len)
    {
        if (string.IsNullOrEmpty(s))
            return s;

        if (s.Length < len - 3)
            return s;

        return s.Substring(0, len - 3) + "...";
    }

    public static string ToPascalCase(this string word)
    {
        return string.Join("", word.Split('_')
            .Select(w => w.Trim())
            .Where(w => w.Length > 0)
            .Select(w => w.Substring(0, 1).ToUpper() + w.Substring(1).ToLower()));
    }

    public static string[] Split(this string s, params (int offset, int len)[] metrics)
    {
        var res = new List<string>();
        foreach (var (offset, len) in metrics)
        {
            if (s.Length >= offset + len)
                res.Add(s.Substring(offset, len));
        }

        return res.ToArray();
    }

    public static byte[] ToBytes(this string s, Encoding? encoding = null)
    {
        return (encoding ?? Encoding.UTF8).GetBytes(s);
    }

    public static string RemoveHtmlTags(this string s)
    {
        return System.Text.RegularExpressions.Regex.Replace(
            HtmlTags.Aggregate(s, (val, tuple) => val.Replace(tuple.Item1, tuple.Item2)),
            "<.*?>", String.Empty);
    }

    public static string ToHtml(this string s)
    {
        return HtmlTags.Aggregate(s, (val, tuple) => val.Replace(tuple.Item2, tuple.Item1));
    }
}