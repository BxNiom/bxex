namespace BxEx;

public static class ByteArrayEx
{
    public static short ToInt16(this byte[] ar, int offset)
    {
        return BitConverter.ToInt16(ar, offset);
    }

    public static int ToInt32(this byte[] ar, int offset)
    {
        return BitConverter.ToInt32(ar, offset);
    }

    public static long ToInt64(this byte[] ar, int offset)
    {
        return BitConverter.ToInt64(ar, offset);
    }

    public static ushort ToUInt16(this byte[] ar, int offset)
    {
        return BitConverter.ToUInt16(ar, offset);
    }

    public static uint ToUInt32(this byte[] ar, int offset)
    {
        return BitConverter.ToUInt32(ar, offset);
    }

    public static ulong ToUInt64(this byte[] ar, int offset)
    {
        return BitConverter.ToUInt64(ar, offset);
    }

    public static float ToSingle(this byte[] ar, int offset)
    {
        return BitConverter.ToSingle(ar, offset);
    }

    public static double ToDouble(this byte[] ar, int offset)
    {
        return BitConverter.ToDouble(ar, offset);
    }
}