using System.Collections;

namespace BxEx.Collections;

public class ByteBuffer : IEnumerable<byte>
{
    private byte[] _data;

    public ByteBuffer(IEnumerable<byte> data) : this(data.Count())
    {
        _data = data.ToArray();
    }

    public ByteBuffer(int capacity = 0)
    {
        IsFixedCapacity = capacity != 0;
        Capacity = capacity == 0 ? 8 : capacity;
        Clear();
    }

    public int Capacity { get; private set; }
    public int Position { get; private set; }
    public int Length { get; private set; }
    public bool IsFixedCapacity { get; }

    public byte this[int index]
    {
        get
        {
            if (index >= Length)
                throw new IndexOutOfRangeException();

            return _data[index];
        }
    }

    public IEnumerator<byte> GetEnumerator() => _data.AsEnumerable().GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => _data.AsEnumerable().GetEnumerator();

    public void Clear()
    {
        Position = 0;
        Capacity = IsFixedCapacity ? Capacity : 8;
        _data = new byte[Capacity];
        Array.Fill(_data, (byte)0x00);
    }

    public void Seek(int offset, SeekOrigin origin)
    {
        int newPos = origin switch
        {
            SeekOrigin.Begin => offset,
            SeekOrigin.Current => Position + offset,
            _ => Length + offset
        };

        if (newPos >= Length)
            throw new IndexOutOfRangeException();

        Position = newPos;
    }

    private void EnsureSize(int req)
    {
        if (Position + req >= Capacity)
        {
            if (IsFixedCapacity)
                throw new IndexOutOfRangeException();

            var newCapacity = Capacity;
            while (newCapacity < Position + req)
                newCapacity *= 2;

            var newArray = new byte[newCapacity];

            Array.Copy(_data, 0, newArray, 0, Capacity);
            _data = newArray;
            Capacity = newCapacity;
        }
    }

    private ByteBuffer Write<T>(T v, Func<T, byte[]> converter)
    {
        var bytes = converter(v);
        EnsureSize(bytes.Length);
        Array.Copy(bytes, 0, _data, Position, bytes.Length);
        Position += bytes.Length;
        Length = System.Math.Max(Position, Length);

        return this;
    }

    public ByteBuffer Write(byte v) => Write(v, b => new byte[1] { b });
    public ByteBuffer Write(short v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(int v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(long v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(float v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(double v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(ushort v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(uint v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(ulong v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(char v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(bool v) => Write(v, BitConverter.GetBytes);
    public ByteBuffer Write(byte[] v) => Write(v, 0, v.Length);
    public ByteBuffer Write(Span<byte> v) => Write(v.ToArray(), 0, v.Length);

    public ByteBuffer Write(byte[] v, int offset, int len)
    {
        EnsureSize(len);
        Array.Copy(v, offset, _data, Position, len);
        Position += len;
        Length = System.Math.Max(Position, Length);

        return this;
    }

    private byte[] ReadBytes(int length)
    {
        EnsureSize(length);
        var result = new byte[length];
        Array.Copy(_data, Position, result, 0, length);
        Position += length;
        return result;
    }

    public byte ReadByte() => ReadBytes(1)[0];
    public short ReadInt16() => BitConverter.ToInt16(ReadBytes(sizeof(short)), 0);
    public int ReadInt32() => BitConverter.ToInt32(ReadBytes(sizeof(int)), 0);
    public long ReadInt64() => BitConverter.ToInt64(ReadBytes(sizeof(long)), 0);
    public ushort ReadUInt16() => BitConverter.ToUInt16(ReadBytes(sizeof(ushort)), 0);
    public uint ReadUInt32() => BitConverter.ToUInt32(ReadBytes(sizeof(uint)), 0);
    public ulong ReadUInt64() => BitConverter.ToUInt64(ReadBytes(sizeof(ulong)), 0);
    public float ReadSingle() => BitConverter.ToSingle(ReadBytes(sizeof(float)), 0);
    public double ReadDouble() => BitConverter.ToDouble(ReadBytes(sizeof(double)), 0);
    public char ReadChar() => BitConverter.ToChar(ReadBytes(sizeof(char)), 0);
    public bool ReadBool() => BitConverter.ToBoolean(ReadBytes(sizeof(bool)), 0);

    public byte[] ToArray()
    {
        var res = new byte[Length];
        Array.Copy(_data, 0, res, 0, Length);
        return res;
    }

    public static implicit operator ByteBuffer(byte[] arr) => new(arr);

    public class Enumerator : IEnumerator<byte>
    {
        private readonly ByteBuffer _buffer;
        private int _pos;

        public Enumerator(ByteBuffer buffer)
        {
            _buffer = buffer;
        }

        public bool MoveNext()
        {
            if (_pos + 1 >= _buffer.Length)
                return false;
            _pos++;
            return true;
        }

        public void Reset()
        {
            _pos = 0;
        }

        public byte Current => _buffer._data[_pos];

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }
    }
}