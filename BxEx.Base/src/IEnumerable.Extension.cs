namespace BxEx
{
    public static class MathEx
    {
        public static (byte min, byte max) MinMax(this IEnumerable<byte> e) => (e.Min(), e.Max());
        public static (short min, short max) MinMax(this IEnumerable<short> e) => (e.Min(), e.Max());
        public static (int min, int max) MinMax(this IEnumerable<int> e) => (e.Min(), e.Max());
        public static (long min, long max) MinMax(this IEnumerable<long> e) => (e.Min(), e.Max());
        public static (ushort min, ushort max) MinMax(this IEnumerable<ushort> e) => (e.Min(), e.Max());
        public static (uint min, uint max) MinMax(this IEnumerable<uint> e) => (e.Min(), e.Max());
        public static (ulong min, ulong max) MinMax(this IEnumerable<ulong> e) => (e.Min(), e.Max());
        public static (float min, float max) MinMax(this IEnumerable<float> e) => (e.Min(), e.Max());
        public static (double min, double max) MinMax(this IEnumerable<double> e) => (e.Min(), e.Max());

        public static TAccumulate Aggregate<TSource, TAccumulate>(this IEnumerable<TSource> source,
            TAccumulate seed, Func<TAccumulate, int, TSource, TAccumulate> func)
        {
            var idx = 0;
            return source.Aggregate(seed, (current, s) => func(current, idx++, s));
        }
    }
}