using System.Diagnostics;

namespace BxEx;

public static class ProcessHelper
{
    public static bool TryGetOutput(string executable, string args, out string[] output, int timeout = 5000) =>
        TryGetOutput(executable, args, out output, TimeSpan.FromMilliseconds(timeout));

    public static bool TryGetOutput(string executable, string args, out string[] output, TimeSpan timeout)
    {
        var lines = new List<string>();
        output = Array.Empty<string>();
        try
        {
            using var proc = new Process();
            proc.StartInfo = new ProcessStartInfo
            {
                FileName = executable,
                Arguments = args,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            };

            proc.Start();
            while (!proc.StandardOutput.EndOfStream)
                lines.Add(proc.StandardOutput.ReadLine() ?? "");

            output = lines.ToArray();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
}