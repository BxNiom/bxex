namespace BxEx;

public static class BinaryReaderEx
{
    public static byte PeekByte(this BinaryReader reader) =>
        BitConverter.GetBytes((char)reader.PeekChar())[0];
}