namespace BxEx;

public static class ArrayEx
{
    public static bool Compare<T>(this T[] ar, T[] other, int offset)
    {
        if (ar.Length < other.Length + offset)
            return false;

        for (var i = 0; i < other.Length; i++)
            if (!(ar[i + offset]?.Equals(other[i]) ?? false))
                return false;

        return true;
    }

    public static bool Compare<T>(this T[] ar, T[] other, int offset, int length)
    {
        if (other.Length < offset + length)
            return false;

        if (ar.Length < offset + length)
            return false;

        for (var i = 0; i < length; i++)
            if (!(ar[i]?.Equals(other[i + offset]) ?? false))
                return false;

        return true;
    }

    public static bool Compare<T>(this T[] array, int arrayStartIdx, T[] other, int otherStartIdx, int length)
    {
        if (array.Length < arrayStartIdx + length)
            throw new IndexOutOfRangeException();

        if (other.Length < otherStartIdx + length)
            throw new IndexOutOfRangeException();


        for (var i = 0; i < length; i++)
            if (!(array[i + arrayStartIdx]?.Equals(other[i + otherStartIdx]) ?? false))
                return false;

        return true;
    }
}