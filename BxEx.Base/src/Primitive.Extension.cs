namespace BxEx;

public static class PrimitiveEx
{
    public static byte[] ToBytes(this short v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this int v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this long v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this ushort v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this uint v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this ulong v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this float v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this double v) => BitConverter.GetBytes(v);
    public static byte[] ToBytes(this bool v) => BitConverter.GetBytes(v);

    public static string ToHexString(this byte b)
    {
        return Convert.ToString(b, 16);
    }

    public static bool IsBitSet(this byte b, int bit)
    {
        return (b & (1 << bit)) != 0;
    }

    public static bool IsBitSet(this ushort b, int bit)
    {
        return (b & (1 << bit)) != 0;
    }

    public static bool IsBitSet(this uint b, int bit)
    {
        return (b & (1 << bit)) != 0;
    }

    public static bool IsBitSet(this ulong b, int bit)
    {
        return (b & ((ulong)1 << bit)) != 0;
    }
}