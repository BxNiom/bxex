using System.Xml.Linq;
using System.Xml.XPath;
using log4net;

namespace BxEx.Xml;

public abstract class XDataManager
{
    protected readonly ILog log = LogManager.GetLogger(typeof(XDataManager));

    protected XDataManager(XDocument document)
    {
        Document = document;
        log.Debug("Data manager created");
    }

    public XDocument Document { get; }

    public IXRepository<TEntity> CreateRepository<TEntity>() where TEntity : XEntity
    {
        log.DebugFormat("Create repository for {0}", typeof(TEntity));
        var entityInfo = XEntityManager.GetInfo<TEntity>();
        var root = Document.XPathSelectElement(entityInfo.Attribute.RootPath);

        if (root == null)
            throw new InvalidDataException("Repository root path not found");

        return new XRepository<TEntity>(root);
    }
}