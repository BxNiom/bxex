namespace BxEx.Math;

public static class PrimitiveEx
{
    public static byte Limit(this byte v, byte max) => MathUtils.Limit(v, max);
    public static short Limit(this short v, short max) => MathUtils.Limit(v, max);
    public static int Limit(this int v, int max) => MathUtils.Limit(v, max);
    public static long Limit(this long v, long max) => MathUtils.Limit(v, max);
    public static ushort Limit(this ushort v, ushort max) => MathUtils.Limit(v, max);
    public static uint Limit(this uint v, uint max) => MathUtils.Limit(v, max);
    public static ulong Limit(this ulong v, ulong max) => MathUtils.Limit(v, max);
    public static float Limit(this float v, float max) => MathUtils.Limit(v, max);
    public static double Limit(this double v, double max) => MathUtils.Limit(v, max);

    public static int Clamp(this int d, int min, int max)
    {
        return d < min ? min : d > max ? max : d;
    }

    public static long Clamp(this long d, long min, long max)
    {
        return d < min ? min : d > max ? max : d;
    }

    public static float Clamp(this float d, float min, float max)
    {
        return d < min ? min : d > max ? max : d;
    }

    public static uint Clamp(this uint d, uint min, uint max)
    {
        return d < min ? min : d > max ? max : d;
    }

    public static ulong Clamp(this ulong d, ulong min, ulong max)
    {
        return d < min ? min : d > max ? max : d;
    }
}