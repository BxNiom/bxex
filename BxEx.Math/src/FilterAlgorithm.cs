﻿namespace BxEx.Math;

public enum FilterAlgorithm
{
    None,
    DecayAverage,
    WindowedAverage
}